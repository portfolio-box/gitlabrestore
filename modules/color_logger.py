import logging as log
import sys
import coloredlogs

logger = log.getLogger(__name__)
log.basicConfig()
coloredlogs.install(fmt='%(asctime)s %(levelname)-8s - %(message)s',
                    level="DEBUG", logger=logger, datefmt='%m/%d/%Y %I:%M:%S %p')
