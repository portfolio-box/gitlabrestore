import docker
from modules.remove_cont import remove_cont
from modules.msg_slack import sendmsg
from modules.color_logger import log

client = docker.from_env()


def create_gitlab(imgname, cntname, volcnf, prtcnf):
    try:
        client.containers.run(
            imgname, name=cntname, volumes=volcnf, ports=prtcnf, detach=True, tty=True)
    except:
        sendmsg("Err: Failed to create container!!", "danger")
        log.error("Cleaning up things!!")
        remove_cont(cntname)
        log.info('Done cleaning up things!!')
        exit()
    else:
        # sendmsg(f"Container created with name {cntname}.")
        log.debug(f"Container created with name {cntname}.")
