import docker
from modules.msg_slack import sendmsg
from modules.color_logger import log

client = docker.from_env()


def clean_cont(cntname):
    client.containers.get(cntname).remove(v=True, force=True)
    log.info(f"Cleaning '{cntname}' Container.")


def remove_cont(cntname):
    if client.containers.get(cntname):
        client.containers.get(cntname).remove(v=True, force=True)
        log.error(f"Removing and Cleaning container due to failure.",
                  type="danger")
        sendmsg(f"{cntname} Container removed")
        exit()
