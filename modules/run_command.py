import docker
from modules.remove_cont import remove_cont
from modules.msg_slack import sendmsg
from modules.color_logger import log
import sys

client = docker.from_env(timeout=600)


def run_command(cmd, cntname):
    try:
        containers = client.containers.get(cntname).exec_run(
            cmd, tty=True, socket=True, stream=True, stdin=False, demux=True).output
        for line in containers:
            # print()
            sys.stdout.buffer.write(b'  ')
            # print(line, end='')
            sys.stdout.buffer.write(line)
        return containers
    except:
        log.error("Failed while stopping services in container!!")
        sendmsg(f"Failed while stopping services in container!!", type="danger")
        remove_cont(cntname)
        exit()
