from time import sleep
import docker
import requests as r
from modules.msg_slack import sendmsg
from modules.remove_cont import clean_cont
from modules.color_logger import log

client = docker.APIClient(base_url='unix:///var/run/docker.sock')


def status_check(git, cntname):
    # print(git, cntname)
    log.info(client.inspect_container(
        cntname)['State']['Health']['Status'])
    health_stat = client.inspect_container(
        cntname)['State']['Health']['Status']
    count = 0
    while health_stat == 'starting':
        health_stat = client.inspect_container(
            cntname)['State']['Health']['Status']
        log.info(f'Count:{count+1} Container is not healthy. retrying....')
        count += 1
        sleep(15)


def health_check(git, cntname, pkgname):
    count = 0
    while count < 60:
        gitlab = r.get(git)
        g_statcode = gitlab.status_code
        if g_statcode == 502 or g_statcode == 500:
            log.info(
                f" Count:{count+1} GitLab is offline with status_code--> {g_statcode} retrying...")
            count += 1
            sleep(15)
            if count >= 60:
                break
        elif g_statcode == 200:
            log.info(
                f"7) Restoring of backup was successful. status_code: {g_statcode}")
            # log.debug(f"Success: status_code--> {g_statcode}")
            sendmsg(
                f"7) Restoring of backup was successful. status_code: {g_statcode}")
            clean_cont(cntname)
            sendmsg(f"*'{pkgname}'* is healthy.")
            break
        else:
            log.error(
                f"7) Restoring of backup failed. status_code: {g_statcode}")
            # log.error(f"Failed: status_code--> {g_statcode}")
            sendmsg(
                f"7) Restoring of backup failed. status_code: {g_statcode}", "danger")
            break
