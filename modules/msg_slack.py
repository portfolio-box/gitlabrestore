from slack import WebClient
import configparser
from pathlib import Path

cd = Path().resolve()
# print(f"{str(cd)}/config.ini")
config = configparser.ConfigParser()
config.read(f"{str(cd)}/config.ini")

Token = config['SLACK']['Token']

slkclient = WebClient(token=Token)

channel_name = config['SLACK']['Channel']


def sendmsg(msg, type="success", host=""):
    danger = "#F44336"
    warning = "#FFEB3B"
    success = "#4CAF50"
    if not host:
        if type == "danger":
            color = danger
            sign = "🚨"
        elif type == "warning":
            color = warning
            sign = ":warning:"
        else:
            color = success
            sign = "✔️"

        slkclient.chat_postMessage(
            channel=channel_name,
            attachments=[
                {
                    "color": color,
                    "text": f"{msg} {sign}"
                }
            ]
        )
    else:
        if type == "danger":
            color = danger
            sign = "🚨"
        elif type == "warning":
            color = warning
            sign = ":warning:"
        else:
            color = success
            sign = "💯"

        slkclient.chat_postMessage(
            channel=channel_name,
            attachments=[
                {
                    "color": color,
                    "text": f"{msg} {sign} ---> {host}"
                }
            ]
        )
