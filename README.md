# GitLabRestore

## Python Script that Downloads GitLab archive Package from S3 and Check if backup is healthy  by running it inside a gitlab container locally.

### Note: `It requires docker in your host and slack token to send status of completion and failure in slack channel.`

### Steps followed by program to check if backup archive is healthy:
1.  It checks today's uploaded backup
2.  Pull's archive from s3
3.  Pull's gitlab container image with respect to the version of backup it had pulled
4.  Spin's gitlab container mounting the backup and exposes port
5.  Stops gitlab services
6.  Executes gitlab restore commands and follows the process
7.  Starts the services
8.  Checks by requesting login page repeatedly if gitlab has started properly with provided backup else fails
9.  Returns status to slack

### Things to follow before running this program:
1.  Install docker
2.  Copy Sample-config.ini to config.ini
3.  Provide Slack token in config.ini
4.  Provide Slack channel in config.ini
5.  Provide S3 Bucket Name in config.ini
6.  Install required programs in requirements.txt (`pip install -r requirements.txt`)
