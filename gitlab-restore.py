# !/bin/python3
from modules.create_gitlab import create_gitlab
from modules.remove_cont import remove_cont
from modules.run_command import run_command
from modules.health_gitlab import health_check, status_check
from modules.msg_slack import sendmsg
from modules.color_logger import log
import os
import datetime
import re
import boto3 as aws
import sys
import configparser
from pathlib import Path

cd = Path().resolve()
config = configparser.ConfigParser()
config.read(f"{str(cd)}/config.ini")

port = config['DOCKER']['Port']
pwd = os.getcwd()
today = datetime.date.today()
s3 = aws.resource('s3')
# sys.stdout = open('file.log', 'w')

# Docker Variables
portconf = {'80/tcp': port}
volconf = {f'{pwd}/gitlab_backup':
           {'bind': '/var/opt/gitlab/backups', 'mode': 'rw'}}
contname = config['DOCKER']['ContainerName']

giturl = 'http://localhost:9999'

bucketname = config['AWSS3']['BucketName']

ilm_backup = s3.Bucket(bucketname)
today_date = today.strftime('%Y_%m_%d')

for obj in ilm_backup.objects.all():
    filtered = re.findall(rf".*{today_date}.*", obj.key)
    for backup in filtered:
        try:
            # Getting Latest Backup package from S3 and Extracting version number
            # backup = "1562904267_2019_07_12_12.0.3_gitlab_backup.tar"
            backup_name = backup
            without_ext = backup.split("_gitlab_backup.tar")[0]
            git_ver = backup.split("_")[4]
            log.warning(backup_name)
            log.warning(without_ext)
            log.warning(git_ver)
            # ilm_backup.download_file(backup, f"./gitlab_backup/{backup}")
            log.info(f"1) Downloaded latest backup.")
            sendmsg(f"1) Downloaded latest backup.")

            # Create GitLab Container with the matching backup version
            imgname = f'gitlab/gitlab-ce:{git_ver}-ce.0'
            create_gitlab(imgname, contname, volconf, portconf)
            log.info(f"2) Created container with name {contname}")
            sendmsg(f"2) Created container with name {contname}")

            # Container status check
            status_check(giturl, contname)
            log.info(f"2.1) GitLab properly started on {contname}")
            sendmsg(f"2.1) GitLab properly started on {contname}")

            # Stop all GitLab services inside the container
            command_list = ["gitlab-ctl stop unicorn",
                            "gitlab-ctl stop sidekiq", "gitlab-ctl status"]
            for commands in command_list:
                run_command(commands, contname)
            log.info(f"3) Stopped gitlab services")
            sendmsg(f"3) Stopped gitlab services")

            # Trigger restore command
            restore_cmd = f"gitlab-rake gitlab:backup:restore BACKUP={without_ext} force=yes"
            run_command(restore_cmd, contname)
            log.info(f"4) Restored gitlab backup")
            sendmsg(f"4) Restored gitlab backup")

            # Restart all services
            restart_cmd = "gitlab-ctl restart"
            run_command(restart_cmd, contname)
            log.info(f"5) Restarted gitlab services")
            sendmsg(f"5) Restarted gitlab services")

            # Check if gitlab is running properly
            log.info(
                f"6) Checking gitlab health status might take 15 minutes or less.")
            sendmsg(
                f"6) Checking gitlab health status might take 15 minutes or less.", "warning")
            health_check(giturl, contname, backup_name)
            # sys.stdout.close()
            break

        except:
            # Try block failure handling removes container when failed from above steps.
            sendmsg("Err: Failed while following the steps.", "danger")
            log.error("Err: Failed while following the steps.")
            remove_cont(contname)
            break
